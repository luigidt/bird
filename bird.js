
const PIPE_WIDTH = 25;
const HEIGHT = 400;
const GAP = 100;
const BIRD_RADIUS = 10;

function renderPipes (pipes) {
  // Para cada obstaculo, gera um retangulo superior e um inferior
  const rects = _.flatMap(pipes, box => {
    return [
      // bottom pipe
      {
        x: box.x - PIPE_WIDTH / 2, 
        y: HEIGHT - box.h, 
        h: box.h
      },
      // top pipe
      {
        x: box.x - PIPE_WIDTH / 2, 
        y: 0, 
        h: HEIGHT - box.h - GAP
      },
    ];    
  });
  
  const pipeGroups = d3.select("#pipes")
	.selectAll("rect")
	.data(rects);

  pipeGroups.merge(
    pipeGroups.enter().append("rect"))
    .attr("width", PIPE_WIDTH)
    .attr("height", rect => rect.h)
    .attr("x", rect => rect.x)
    .attr("y", rect => rect.y)
    .attr("class", "pipe");
  
  pipeGroups.exit().remove();
}

function renderBird(bird) {
  d3.select("#bird")
    .attr("cx", bird.x)
    .attr("cy", bird.y)
}

function renderCamera(camera) {
  d3.select("#app")
    .attr("viewBox", camera.x + " -200 1000 800")
}

function render(state) {
  renderPipes(state.pipes);
  renderBird(state.bird);
  renderCamera(state.camera);
}

function createPipes() {
  const randomPosition = d3.randomUniform(150, 240);
  const randomHeight = d3.randomUniform(0, 300);
  
  return _.chain(_.range(10))
    .map(() => {
      return {
        x: randomPosition(), 
        h: randomHeight()
      }
    })
    .update(0, pipe => {
      pipe.x += 150;
      return pipe;
    })
    .reduce((acc, curr) => {
      const total = acc.total + curr.x;      
      var pipe = {
        x: total,
        h: curr.h
      }
      return {
        total: total,
        pipes: acc.pipes.concat([pipe])
      };
    }, {total: 0, pipes: []})
    .get('pipes')
    .value()
}

function createState() {
  return {
    pipes: createPipes(),
    bird: {
      x: 0,
      y: 0,
      dy: 3,
      dx: 2
    },
    camera: {
      x: 0,
      zoom: 1
    }
  };
}

function birdRight(bird) {
  return bird.x + BIRD_RADIUS;
}

function birdLeft(bird) {
  return bird.x - BIRD_RADIUS;
}

function pipeLeft(pipe) {
  return pipe.x - PIPE_WIDTH / 2;
}

function pipeRight(pipe) {
  return pipe.x + PIPE_WIDTH / 2;
}

function birdTop(bird) {
  return bird.y - BIRD_RADIUS;
}

function birdBottom(bird) {
  return bird.y + BIRD_RADIUS;
}

function pipeBottom(pipe) {
  return HEIGHT - pipe.h - GAP;
}

function pipeTop(pipe) {
  return HEIGHT - pipe.h;
}

function isColliding({bird, pipes}) {
  return _.some(pipes, pipe => {
    // passou pela margem esquerda do cano
    if (birdRight(bird) > pipeLeft(pipe) && birdLeft(bird) < pipeRight(pipe)) {
      // se estiver na altura do cano de cima
      if (birdTop(bird) < pipeBottom(pipe)) {
      	return true;
      }
      
      // se estiver na altura do cano de baixo
      if (birdBottom(bird) > pipeTop(pipe)) {
      	return true;
      }            
    }
    
    return false;  
  })
}

function nextState(state) {
  //state.bird.dy = Math.min(7, state.bird.dy + 1);
  
  state.bird.x += state.bird.dx;
  state.bird.y =  Math.max(0, state.bird.y + Math.min(3, state.bird.dy));
  
  state.bird.dy = Math.min(
    7, 
    state.bird.dy > 0 ?
      state.bird.dy * 2 :
      state.bird.dy + 1
  );
  
  if (state.bird.x > 150) {
    state.camera.x = state.bird.x - 150;
  }
  
  if (mouseDown) {
    state.bird.dy = -7;
  }
  
  return state;
}

function isOut(state) {
  return birdBottom(state.bird) > 400
}

function isDone(state) {
  return isColliding(state) || isOut(state);
}

function predict(state) {
  const remaining = getReaminingPipes(state);
  const birdY = HEIGHT - state.bird.y;
  const currentPipeHeight = remaining[0].h + GAP / 2;  
  const distanceToCurrentPipe = remaining[0].x - state.bird.x;
  
  const tensor = tf.tensor2d([
    birdY, 
    currentPipeHeight, 
    distanceToCurrentPipe
  ], [1, 3]);

  return model.predict(tensor)
    .data()
    .then(x => x[0]);
}

function getReaminingPipes(state) {
  return _.filter(state.pipes, pipe => pipeLeft(pipe) > birdRight(state.bird))
}

function loop(currState) {
  predict(currState).then(x => {
    mouseDown = x > 0.5;
    
    const state = nextState(_.cloneDeep(currState));
    render(state);
    
    if (!isDone(state)) {
      window.requestAnimationFrame(loop.bind(null, state));
    } else {
      const remaining = getReaminingPipes(state);
      
      if (remaining.length > 0) {
      	const remaining = getReaminingPipes(state);
	const birdY = HEIGHT - state.bird.y;
  	const currentPipeHeight = remaining[0].h + GAP / 2;  
	const distanceToCurrentPipe = remaining[0].x - state.bird.x;
        
        const xs = tf.tensor2d(
          [
            birdY, 
            currentPipeHeight, 
            distanceToCurrentPipe,
          ], 
          [1, 3]);
        
        const bias = (currentPipeHeight - birdY) / 400 + 0.45;

        
        const ys = tf.tensor2d(
          [
	    bias
          ], [1, 1]);

        model.fit(xs, ys, {epochs: 10}).then(() => {
          window.requestAnimationFrame(loop.bind(null, createState()));
        })          
      }
    }
  });
}

var testState = {
  pipes: [{x: 18, h: 100}],
  bird: {
    x: 0,
    y: 50
  },
  camera: {x: 0}
};

if (!isColliding(testState)) {
  render(testState);
  throw new Error("Deveria estar colidindo!");
}

testState = {
  pipes: [{x: 18, h: 100}],
  bird: {
    x: 0,
    y: 350
  },
  camera: {x: 0}
};

if (!isColliding(testState)) {
  render(testState);
  throw new Error("Deveria estar colidindo!");
}

testState = {
  pipes: [{x: 23, h: 100}],
  bird: {
    x: 0,
    y: 350
  },
  camera: {x: 0}
};

if (isColliding(testState)) {
  render(testState);
  throw new Error("Não deveria estar colidindo!");
}

testState = {
  pipes: [{x: 23, h: 100}],
  bird: {
    x: 0,
    y: 20
  },
  camera: {x: 0}
};

if (isColliding(testState)) {
  render(testState);
  throw new Error("Não deveria estar colidindo!");
}

testState = {
  pipes: [{x: 23, h: 100}],
  bird: {
    x: 23,
    y: 230
  },
  camera: {x: 0}
};

if (isColliding(testState)) {
  render(testState);
  throw new Error("Não deveria estar colidindo!");
}

testState = {
  pipes: [{x: 23, h: 100}],
  bird: {
    x: 60,
    y: 100
  },
  camera: {x: 0}
};

if (isColliding(testState)) {
  render(testState);
  throw new Error("Não deveria estar colidindo!");
}

testState = {
  pipes: [{x: 23, h: 100}],
  bird: {
    x: 60,
    y: 350
  },
  camera: {x: 0}
};

if (isColliding(testState)) {
  render(testState);
  throw new Error("Não deveria estar colidindo!");
}

testState = {
  pipes: [{x: 23, h: 100}],
  bird: {
    x: 23,
    y: 293
  },
  camera: {x: 0}
};

if (!isColliding(testState)) {
  render(testState);
  throw new Error("Deveria estar colidindo!");
}

testState = {
  pipes: [{x: 23, h: 100}],
  bird: {
    x: 23,
    y: 209
  },
  camera: {x: 0}
};

if (!isColliding(testState)) {
  render(testState);
  throw new Error("Deveria estar colidindo!");
}

var mouseDown = 0;

function createModel() {
  const NEURONS = 6; 
  const hiddenLayer = tf.layers.dense({ 
    units: NEURONS,
    inputShape: [3], 
    activation: 'sigmoid', 
    kernelInitializer: 'leCunNormal', 
    useBias: true, 
    biasInitializer: tf.initializers.constant({ 
      value: d3.randomUniform(-2, 2)(), 
    }),
  }); 
  const outputLayer = tf.layers.dense({ units: 1, });

  const model = tf.sequential(); 
  model.add(hiddenLayer); 
  model.add(outputLayer);
  model.compile({ loss: 'meanSquaredError', optimizer: 'sgd' });
  return model;
}

const model = createModel()

const xs = tf.tensor2d(
  [
    0, 400, 35,
    200, 200, 35,
    400, 0, 35,    
  ], 
  [3, 3]);
const ys = tf.tensor2d(
  [
    1,
    0.5,
    0    
  ], [3, 1]);

model.fit(xs, ys, {epochs: 10}).then(() => {
  window.requestAnimationFrame(loop.bind(null, createState()));
})








